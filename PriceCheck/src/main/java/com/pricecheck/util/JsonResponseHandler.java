package com.pricecheck.util;

import android.util.Log;
import com.pricecheck.model.Product;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 2/25/14.
 */
public class JsonResponseHandler
{
    private static String TAG = "com.pricecheck.util.ProductJsonResponseHandlerjava";

    public static List<List<Product>> handleProductsResponse(String JSONResponse)
    {
        List<List<Product>> ret = new ArrayList<List<Product>>();
        List<Product> result = new ArrayList<Product>();
        ret.add(result);
        List<Product> autoSuggest = new ArrayList<Product>();
        ret.add(autoSuggest);

        if(JSONResponse == null)
            return null;
        try
        {
            JSONArray combinedResponse = (JSONArray) new JSONTokener((JSONResponse)).nextValue();

            for(int i = 0; i < combinedResponse.length(); i++)
            {
                JSONObject siteResponse = (JSONObject) combinedResponse.get(i);
                String siteResponseName = (String) siteResponse.getString("siteName");
                JSONArray products = siteResponse.getJSONArray("products");

                for(int j = 0; j < products.length(); j++)
                {
                    JSONObject product = (JSONObject) products.get(j);

                    String name = product.getString("name"),
                            price = product.getString("price"),
                            imageUrl = product.getString("imageUrl"),
                            productLink = product.getString("productLink"),
                            siteName = product.getString("siteName");

                    Product newProduct = new Product(name, price, siteName, productLink, imageUrl);
                    if(siteResponseName.equals("AutoSuggest"))
                        autoSuggest.add(newProduct);
                    else
                        result.add(newProduct);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSONException");
        }
        return ret;
    }

    public static List<String> handleAutoCompleteResponse(String JSONResponse)
    {
        List<String> result = new ArrayList<String>();

        try
        {
            JSONObject response = (JSONObject) new JSONTokener(JSONResponse).nextValue();

            String query = (String)response.names().get(0);

            JSONArray primaryArray = (JSONArray) response.get(query);

            if((primaryArray.getString(0)).equals(query))
                primaryArray = (JSONArray) primaryArray.get(1);

            for(int i = 0; i < primaryArray.length(); i++)
                result.add(primaryArray.getString(i));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return result;
    }
}
