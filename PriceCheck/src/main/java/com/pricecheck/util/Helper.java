package com.pricecheck.util;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Surface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pricecheck.R;
import com.pricecheck.SearchActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vivek on 3/1/14.
 */
public class Helper
{
    public static enum SITES {
        FLIPKART, AMAZON, TRADUS, SNAPDEAL, EBAY, INFIBEAM, NAAPTOL, HOMESHOP18
    }
    public static Map<SITES, Integer> drawableIdForSite;

    static {
        drawableIdForSite = new HashMap<SITES, Integer>();
        drawableIdForSite.put(SITES.FLIPKART, R.drawable.ic_flipkart);
        drawableIdForSite.put(SITES.AMAZON, R.drawable.ic_amazon);
        drawableIdForSite.put(SITES.TRADUS, R.drawable.ic_tradus);
        drawableIdForSite.put(SITES.SNAPDEAL, R.drawable.ic_snapdeal);
        drawableIdForSite.put(SITES.EBAY, R.drawable.ic_ebay);
        drawableIdForSite.put(SITES.INFIBEAM, R.drawable.ic_infibeam);
        drawableIdForSite.put(SITES.NAAPTOL, R.drawable.ic_naaptol);
        drawableIdForSite.put(SITES.HOMESHOP18, R.drawable.ic_homeshop18);
    }

    public static boolean isNetworkAvailable(ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static List<String> getQueriesFromString(String queriesString) {
        if(queriesString == null) {
            return new ArrayList<String>(0);
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        List<String> queries;
        try{
            queries = gson.fromJson(queriesString, List.class);
        } catch (Exception e) {
            queries = null;
        }
        return queries;
    }

    public static String getStringFromQueries(List<String> queries) {
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();
        String ret;
        try{
            ret = gson.toJson(queries);
        } catch (Exception e) {
            ret = gson.toJson(new ArrayList<String>(0));
        }
        return ret;
    }

    public static void lockScreenRotation(SearchActivity parentActivity) {

        final int orientation = parentActivity.getResources().getConfiguration().orientation;
        final int rotation = parentActivity.getWindowManager().getDefaultDisplay()
                .getOrientation();

        int SCREEN_ORIENTATION_REVERSE_LANDSCAPE = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
        int SCREEN_ORIENTATION_REVERSE_PORTRAIT = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                parentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                parentActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } else if (rotation == Surface.ROTATION_180
                || rotation == Surface.ROTATION_270) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                parentActivity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_PORTRAIT);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                parentActivity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
            }
        }
    }

    public static void unLockScreenRotation(SearchActivity parentActivity)
    {
        parentActivity.setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    public static int getDrawableIdForSite(SITES site)
    {
        return drawableIdForSite.get(site);
    }
}
