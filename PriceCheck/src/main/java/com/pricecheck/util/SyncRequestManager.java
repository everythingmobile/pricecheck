package com.pricecheck.util;

import android.util.Log;
import com.pricecheck.model.OutboundRequest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by vivek on 3/1/14.
 */
public class SyncRequestManager
{
    private HttpURLConnection httpURLConnection;
    private String TAG = "com.pricecheck.util.RestRequestTask";

    public String getSyncResponse(OutboundRequest outboundRequest)
    {
        String response = null;
        try
        {
            String url = outboundRequest.build();
            httpURLConnection = (HttpURLConnection) new URL(url).openConnection();

            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

            response = readStream(in);
        }
        catch (MalformedURLException e)
        {
            Log.e(TAG, "MalformedURL");
        }
        catch (IOException e)
        {
            Log.e(TAG, "IOException");
        }
        finally
        {
            if(httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return response;
    }

    private String readStream(InputStream in)
    {
        BufferedReader reader = null;
        StringBuffer data = new StringBuffer("");
        try
        {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null)
                data.append(line);
        }
        catch (IOException e)
        {
            Log.e(TAG, "IOException");
        }
        finally
        {
            if(reader != null)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    Log.e(TAG, "IOException");
                }
            }
        }
        return data.toString();
    }
}
