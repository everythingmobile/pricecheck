package com.pricecheck.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by vivek on 2/28/14.
 */
public class ToastCreater
{
    public static void showToast(Context context, String message, int duration)
    {
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
