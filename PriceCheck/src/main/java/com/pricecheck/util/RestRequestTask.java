package com.pricecheck.util;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.pricecheck.model.OutboundRequest;
import com.squareup.okhttp.OkHttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by vivek on 2/25/14.
 */
public class RestRequestTask extends AsyncTask<OutboundRequest, Integer, String> {
    private HttpURLConnection httpURLConnection;
    private String TAG = "com.pricecheck.util.RestRequestTask";
    private Handler foreignHandler;
    private Integer index;

    public RestRequestTask(Handler foreignHandler, int index) {
        this.foreignHandler = foreignHandler;
        this.index = index;
    }

    public RestRequestTask(Handler handler) {
        foreignHandler = handler;
    }

    @Override
    protected String doInBackground(OutboundRequest... params) {
        String response = null;
        if(params.length > 1) {
            return null;
        }
        for(OutboundRequest outboundRequest : params) {
            try {
                OkHttpClient client = new OkHttpClient();
                httpURLConnection = client.open(new URL(outboundRequest.build()));
                InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
                response = readStream(in);
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURL");
            } catch (IOException e) {
                Log.e(TAG, "IOException");
            } finally {
                if(httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
        }
        return response;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer data = new StringBuffer("");
        try  {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                data.append(line);
            }
        } catch (IOException e)  {
            Log.e(TAG, "IOException");
        } finally {
            if(reader != null)  {
                try  {
                    reader.close();
                } catch (IOException e)  {
                    Log.e(TAG, "IOException");
                }
            }
        }
        return data.toString();
    }

    @Override
    protected void onPostExecute(String products) {
        Message message = new Message();
        message.obj = products;
        if(index != null) {
            message.arg1 = index;
        }
        foreignHandler.sendMessage(message);
    }
}
