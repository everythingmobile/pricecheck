package com.pricecheck;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;


public class MainActivity extends Activity {

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Check if the app has been run before
        SharedPreferences prefs = this.getSharedPreferences(
                Constants.FIRST_RUN, Context.MODE_PRIVATE);
        boolean firstRun = prefs.getBoolean(Constants.FIRST_RUN_KEY,true);

        if (firstRun) {
            // Go to the user info activity
            startActivity(new Intent(this, UserTourActivity.class));
            prefs.edit().putBoolean(Constants.FIRST_RUN_KEY,false);
        }
        else {
            // Start and intent for the search activity
            startActivity(new Intent(this, SearchActivity.class));
        }
        finish();
    }

}
