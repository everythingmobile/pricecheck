package com.pricecheck.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pricecheck.R;
import com.pricecheck.SearchActivity;
import com.pricecheck.util.Helper;

/**
 * Created by vivek on 3/1/14.
 */
public class SearchFragment extends android.support.v4.app.Fragment {
    private static SearchActivity parentActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        Helper.lockScreenRotation(parentActivity);
        return inflater.inflate(R.layout.search_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parentActivity.getActionBar().setNavigationMode(ActionBar.DISPLAY_HOME_AS_UP);
    }

    public static SearchFragment newInstance(SearchActivity parentActivity) {
        SearchFragment searchFragment = new SearchFragment();
        searchFragment.setParentActivity(parentActivity);
        return searchFragment;
    }

    public void setParentActivity(SearchActivity parentActivity) {
        this.parentActivity = parentActivity;
    }
}