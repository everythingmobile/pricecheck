package com.pricecheck.fragments;

import android.app.ActionBar;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android.widget.SpinnerAdapter;
import com.pricecheck.Constants;
import com.pricecheck.SearchActivity;
import com.pricecheck.adapters.*;
import com.pricecheck.model.OutboundRequest;
import com.pricecheck.model.Product;
import com.pricecheck.R;
import com.pricecheck.util.*;

import java.util.*;

/**
 * Created by vivek on 3/1/14.
 */
public class ResultsFragment extends Fragment {

    private RelativeLayout parentView;
    private ResultsFragment thisFragment;
    private GridView gridView;
    private ProgressDialog progressDialog;
    private static SearchActivity parentActivity;
    RestRequestTask restRequestTask;
    private List<Product> products;
    private int pageToLoadNext = 1;
    private String currentQuery;
    private final String TAG = "com.pricecheck.ResultsFragment";
    private ProductAdapter productAdapter;
    private SpinnerAdapter mSpinnerAdapter;
    private ActionBar.OnNavigationListener mOnNavigationListener;

    private Handler taskCompleteHandler = new Handler() {
        @Override
        public void handleMessage(Message msg)
        {
            List<Product> productList, autoSuggestList, filteredProducts;

            List<List<Product>> response = JsonResponseHandler.handleProductsResponse((String) msg.obj);
            if(response == null || response.size() < 2)
            {
                linlaHeaderProgress.setVisibility(View.GONE);
                return;
            }
            productList = response.get(0);
            filteredProducts = parentActivity.getFilteredProducts(productList);
            autoSuggestList = response.get(1);
            parentActivity.setSupportProgressBarIndeterminateVisibility(false);
            //progressDialog.hide();
            linlaHeaderProgress.setVisibility(View.GONE);

            if(productList == null) {
                return;
            }

            products = productList;

            if(productAdapter == null) {
                productAdapter = new ProductAdapter(parentActivity, products, thisFragment, filteredProducts);
                if (productAdapter.getAutoSuggestList() == null) {
                    productAdapter.setAutoSuggestList(autoSuggestList);
                    productAdapter.setMorePressed(false);
                }
                gridView.setAdapter(productAdapter);
            }
            else  {
                productAdapter.addProducts(productList);
                productAdapter.setMorePressed(false);
                productAdapter.notifyDataSetChanged();
            }

            parentActivity.registerProductAdapter(productAdapter);

            pageToLoadNext++;
        }
    };
    private LinearLayout linlaHeaderProgress;


    public static final ResultsFragment newInstance(SearchActivity parentActivity)
    {
        ResultsFragment resultsFragment = new ResultsFragment();
        resultsFragment.parentActivity = parentActivity;
        return resultsFragment;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        Helper.unLockScreenRotation(parentActivity);
        if (getArguments() != null) {
            currentQuery = getArguments().getString(Constants.SEARCH_BUNDLE_KEY);
            addProductToRecent(currentQuery);
            if(productAdapter == null) {
                getProducts(currentQuery);
            } else {
                this.gridView.setAdapter(productAdapter);
            }
        }  else {
            Log.e(TAG,"The user arguments are not set for the fragment");
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        final SupportMenuItem searchMenuItem = ((SupportMenuItem) menu.findItem(R.id.search));
        if (searchMenuItem.isActionViewExpanded()) {
            searchMenuItem.collapseActionView();
        }

        SearchView searchView = null;

        if (searchMenuItem != null) {
            searchView = (SearchView) searchMenuItem.getActionView();
            searchView.clearFocus();
        }

        else {
            Log.e(TAG, "searchView is null");
        }

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.thisFragment = this;
        parentView = (RelativeLayout) inflater.inflate(R.layout.results_fragment, container, false);
        linlaHeaderProgress = (LinearLayout) parentView.findViewById(R.id.linlaHeaderProgress);
        linlaHeaderProgress.setVisibility(View.GONE);
        setHasOptionsMenu(true);
        getActivity().getActionBar().setNavigationMode(android.support.v7.app.ActionBar.NAVIGATION_MODE_LIST);

        gridView = (GridView) parentView.findViewById(R.id.productsView);


        mSpinnerAdapter = new com.pricecheck.adapters.SpinnerAdapter(parentActivity.getApplicationContext(), R.layout.spinner_element, null);

        mOnNavigationListener = new ActionBar.OnNavigationListener() {
            // Get the same strings provided for the drop-down's ArrayAdapter
            String[] strings = getResources().getStringArray(R.array.action_list);

            @Override
            public boolean onNavigationItemSelected(int position, long itemId) {
                if (productAdapter != null) {
                    productAdapter.sortByPrice(position);
                }
                return true;
            }
        };


        getActivity().getActionBar().setListNavigationCallbacks(mSpinnerAdapter, mOnNavigationListener);
        return parentView;
    }

    private void addProductToRecent(String currentQuery)
    {
        if(parentActivity == null) {
            return;
        }

        SharedPreferences preferences = parentActivity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String queriesString = preferences.getString(Constants.recentString, null);

        List<String> queries = Helper.getQueriesFromString(queriesString);
        if (queries.contains(currentQuery) && !currentQuery.isEmpty()) {
            //Make it the most recent one
            queries.remove(currentQuery);
            queries.add(currentQuery);
        }
        else {

            if(queries == null) {
                return;
            }

            if(queries.size() > 20) {
                queries.remove(0);
            }

            queries.add(currentQuery);
        }
        String stringToBeWritten = Helper.getStringFromQueries(queries);
        editor.putString(Constants.recentString, stringToBeWritten);
        editor.commit();
    }

    public void getProducts(String productName)
    {
        if(!checkNetwork()) {
            return;
        }

        if (pageToLoadNext > 1) {
            parentActivity.setProgressBarIndeterminateVisibility(true);
        }

        if (pageToLoadNext <= 1) {
            showProgressBar();
        }

        if(productName == null) {
            productName = currentQuery;
        }

        OutboundRequest outboundRequest = new OutboundRequest();
        outboundRequest.setHost("ec2-54-255-169-135.ap-southeast-1.compute.amazonaws.com:8080");
        outboundRequest.setPath("CompareNBuy/products");
        outboundRequest.addParameter("q", productName);
        outboundRequest.addParameter("page", Integer.toString(pageToLoadNext));
        restRequestTask = (RestRequestTask) new RestRequestTask(taskCompleteHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, outboundRequest);

    }

    private void showProgressBar() {
        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }

    private boolean checkNetwork()
    {
        if(!Helper.isNetworkAvailable((ConnectivityManager) parentActivity.getSystemService(Context.CONNECTIVITY_SERVICE)))
        {
            ToastCreater.showToast(parentActivity.getApplicationContext(), "Network Unavailable", Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

}