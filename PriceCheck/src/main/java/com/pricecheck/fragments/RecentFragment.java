package com.pricecheck.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.pricecheck.R;
import com.pricecheck.SearchActivity;
import com.pricecheck.adapters.RecentAdapter;
import com.pricecheck.util.Helper;

/**
 * Created by vivek on 4/1/14.
 */
public class RecentFragment extends ListFragment
{

    private static SearchActivity parentActivity;
    private ListView parentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Helper.unLockScreenRotation(parentActivity);
        parentView = (ListView) inflater.inflate(R.layout.recent_view, container, false);
        parentView.setAdapter(new RecentAdapter(parentActivity.getApplicationContext(), R.layout.recent_list_item, parentActivity));
        parentActivity.getActionBar().setNavigationMode(ActionBar.DISPLAY_HOME_AS_UP);
        return parentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public static RecentFragment newInstance(SearchActivity parentActivity)
    {
        RecentFragment recentFragment = new RecentFragment();
        recentFragment.parentActivity = parentActivity;
        return recentFragment;
    }
}
