package com.pricecheck;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.pricecheck.Service.FilteringService;
import com.pricecheck.adapters.LeftNavDrawerAdapter;
import com.pricecheck.adapters.RightNavDrawerAdapter;
import com.pricecheck.adapters.ProductAdapter;
import com.pricecheck.fragments.RecentFragment;
import com.pricecheck.fragments.ResultsFragment;
import com.pricecheck.fragments.SearchFragment;
import com.pricecheck.model.RightNavDrawerItem;
import com.pricecheck.model.Product;
import com.pricecheck.util.Helper;
import com.pricecheck.util.ToastCreater;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends ActionBarActivity
{
    private DrawerLayout mDrawerLayout;
    private ListView mSellerDrawer;

    private CharSequence mDrawerTitle;
    private ArrayList<RightNavDrawerItem> sellerNavDrawerItems;
    private RightNavDrawerAdapter adapter;
    private static FilteringService filteringService;
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private PriceCheckEnum currentFragment;
    private ListView mLeftDrawer;
    private LeftNavDrawerAdapter mLeftDrawerAdapter;
    private Fragment searchFragment;

    public static enum PriceCheckEnum {
        HOME, RESULTS, RECENT
    }
    private final String TAG = "com.pricecheck.SearchActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_search);
        this.setSupportProgressBarIndeterminateVisibility(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mTitle = mDrawerTitle = getTitle();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mSellerDrawer = (ListView) findViewById(R.id.right_drawer);
        mLeftDrawer = (ListView) findViewById(R.id.left_drawer);

        sellerNavDrawerItems = new ArrayList<RightNavDrawerItem>();

        for(Helper.SITES site : Helper.SITES.values())
            sellerNavDrawerItems.add(new RightNavDrawerItem(site, Helper.getDrawableIdForSite(site), true));

        mSellerDrawer.setOnItemClickListener(new SlideMenuClickListener());

        filteringService = new FilteringService(sellerNavDrawerItems);

        adapter = new RightNavDrawerAdapter(this, sellerNavDrawerItems);
        mSellerDrawer.setAdapter(adapter);

        mLeftDrawerAdapter = new LeftNavDrawerAdapter(getApplicationContext(), R.layout.left_nav_item);
        mLeftDrawer.setAdapter(mLeftDrawerAdapter);
        mLeftDrawer.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                if (mSellerDrawer != null && mDrawerLayout!=null && drawerView.equals(mLeftDrawer)) {
                    if (mDrawerLayout.isDrawerOpen(mSellerDrawer)) {
                        mDrawerLayout.closeDrawer(mSellerDrawer);
                    }
                }
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        handleIntent(getIntent());

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //set the first run pref to false;
        SharedPreferences prefs = this.getSharedPreferences(
                Constants.FIRST_RUN, Context.MODE_PRIVATE);
        prefs.edit()
                .putBoolean(Constants.FIRST_RUN_KEY,false).
                commit();


        if(!checkNetwork()) {
            return;
        }
        displayFragment(PriceCheckEnum.HOME, null);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            displayFragment(PriceCheckEnum.RESULTS,query);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String data = intent.getDataString();
            Log.d(TAG,"Data in search item clicked is "+data);
            //TODO: Still a bad way to program. calling the same method on both if and else. need to refactor.
            displayFragment(PriceCheckEnum.RESULTS,data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public List<Product> getFilteredProducts(List<Product> productList)
    {
        return this.filteringService.getFilteredProducts(productList);
    }

    public void filterChanged()
    {
        filteringService.filterChangeNotification(sellerNavDrawerItems);
    }

    public void registerProductAdapter(ProductAdapter productAdapter)
    {
        filteringService.registerListener(productAdapter);
    }

    /** Swaps fragments in the main content view */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        switch (position) {
            case 0:
                displayFragment(PriceCheckEnum.HOME,null);
                break;
            case 1:
                displayFragment(PriceCheckEnum.RECENT,null);
                break;
            default:
                Log.e(TAG,"This should not happen");
                break;

        }


        // Highlight the selected item, update the title, and close the drawer
        mLeftDrawer.setItemChecked(position, true);

        mDrawerLayout.closeDrawer(mLeftDrawer);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {

        }
    }

    public void displayFragment(PriceCheckEnum priceCheckEnum, String data)
    {
        Fragment fragment = null;
        switch (priceCheckEnum) {
            case HOME:
                if (searchFragment == null) {
                    searchFragment = SearchFragment.newInstance(this);
                }
                fragment = searchFragment;
                break;
            case RESULTS:
                fragment = ResultsFragment.newInstance(this);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.SEARCH_BUNDLE_KEY,data);
                fragment.setArguments(bundle);
                break;
            case RECENT:
                fragment = RecentFragment.newInstance(this);
            default:
                break;
        }

        if(fragment != null)
        {
            fragment.setRetainInstance(true);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFragment, fragment);
            if(currentFragment != PriceCheckEnum.RECENT && (currentFragment != PriceCheckEnum.HOME || priceCheckEnum != PriceCheckEnum.HOME) && currentFragment != null)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            currentFragment = priceCheckEnum;
        }
    }

    private boolean checkNetwork()
    {
        if(!Helper.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)))
        {
            ToastCreater.showToast(getApplicationContext(), "Network Unavailable", Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        final SupportMenuItem searchMenuItem = ((SupportMenuItem) menu.findItem(R.id.search));
        SupportMenuItem filterSettingsItem = (SupportMenuItem) menu.findItem(R.id.filter_settings);
        filterSettingsItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout != null) {
                    if (mLeftDrawer!=null && drawerLayout.isDrawerOpen(mLeftDrawer)) {
                        drawerLayout.closeDrawer(mLeftDrawer);
                    }
                    if (!drawerLayout.isDrawerOpen(mSellerDrawer)) {
                        drawerLayout.openDrawer(mSellerDrawer);
                    } else {
                        drawerLayout.closeDrawer(mSellerDrawer);
                    }
                }
                return true;
            }
        });

        searchMenuItem.collapseActionView();


        SearchView searchView = null;

        if (searchMenuItem != null) {
            searchView = (SearchView) searchMenuItem.getActionView();
            searchView.clearFocus();
        }

        else {
            Log.e(TAG, "searchView is null");
        }

        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
        }
        else {
            Log.e(TAG, "searchView is null");
        }


        if (searchView != null) {
            final SearchView finalSearchView = searchView;
            searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean queryTextFocused) {
                    if(!queryTextFocused) {
                        searchMenuItem.collapseActionView();
                        finalSearchView.setQuery("", false);
                    }
                }
            });
        }

        InputMethodManager imm = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

        return super.onCreateOptionsMenu(menu);

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


}
