package com.pricecheck.contentprovider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.BaseColumns;
import android.util.Log;
import com.pricecheck.model.OutboundRequest;
import com.pricecheck.util.JsonResponseHandler;
import com.pricecheck.util.RestRequestTask;
import com.pricecheck.util.SyncRequestManager;

import java.util.ArrayList;
import java.util.List;

public class ProductsSuggestionProvider extends ContentProvider {
    private static final String LOG_TAG = "com.pricecheck.contentprovider.ProductsSuggestionProvider";

    public static final String AUTHORITY = "com.pricemash.search_suggestion_provider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/search");

    // UriMatcher constant for search suggestions
    private static final int SEARCH_SUGGEST = 1;

    private RestRequestTask restRequestTask;
    private static List<String> suggestionList;

    private static Handler requestCompleteHandler = new Handler() {
        @Override
        public void handleMessage(Message msg)
        {
            suggestionList = (ArrayList<String>) msg.obj;

        }
    };


    private static final UriMatcher uriMatcher;

    private static final String[] SEARCH_SUGGEST_COLUMNS = {
            BaseColumns._ID,
            SearchManager.SUGGEST_COLUMN_TEXT_1,
            SearchManager.SUGGEST_COLUMN_INTENT_DATA
    };

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST);
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST);
    }

    @Override
    public int delete(Uri uri, String arg1, String[] arg2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                return SearchManager.SUGGEST_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues arg1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Log.d(LOG_TAG, "query = " + uri);
        String userQuery = uri.getLastPathSegment().toLowerCase();


        if(userQuery.equals("") || userQuery == null)
            return null;

        // Use the UriMatcher to see what kind of query we have
        switch (uriMatcher.match(uri)) {
            case SEARCH_SUGGEST:
                Log.d(LOG_TAG, "Search suggestions requested.");
                MatrixCursor cursor = new MatrixCursor(SEARCH_SUGGEST_COLUMNS, 1);
                OutboundRequest outboundRequest = new OutboundRequest();
                outboundRequest.setHost("www.flipkart.com");
                outboundRequest.setPath("s");
                outboundRequest.addParameter("query", userQuery);
                String data = new SyncRequestManager().getSyncResponse(outboundRequest);
                suggestionList = JsonResponseHandler.handleAutoCompleteResponse(data);

                if (suggestionList == null) {
                    Log.e(LOG_TAG,"Response is NULL");
                }
                else {
                    for (String s: suggestionList) {
                        String[] temp = new String[cursor.getColumnCount()];
                        for (int i = 1; i < cursor.getColumnCount(); i++) {
                            temp[cursor.getColumnCount() - i] = s;
                        }
                        cursor.addRow(temp);
                    }
                }
                Log.d(LOG_TAG,"Cursor created is"+cursor);
                return cursor;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues arg1, String arg2, String[] arg3) {
        throw new UnsupportedOperationException();
    }
}