package com.pricecheck.model;

import android.graphics.Bitmap;
import com.pricecheck.util.Helper;

public class RightNavDrawerItem {
	
	private Helper.SITES site;
	private String count = "0";
    private boolean checkBoxState;
    private int drawable;
	// boolean to set visiblity of the counter
	private boolean isCounterVisible = false;

    public RightNavDrawerItem(Helper.SITES site, int drawable, boolean checkBoxState)
    {
        this.site = site;
        this.drawable = drawable;
        this.checkBoxState = checkBoxState;
    }

    public Helper.SITES getSite() {
        return site;
    }

    public String getCount() {
        return count;
    }

    public boolean isCheckBoxState() {
        return checkBoxState;
    }

    public int getDrawable() {
        return drawable;
    }

    public boolean isCounterVisible() {
        return isCounterVisible;
    }

    public void setSite(Helper.SITES site) {
        this.site = site;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setCheckBoxState(boolean checkBoxState) {
        this.checkBoxState = checkBoxState;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public void setCounterVisible(boolean isCounterVisible) {
        this.isCounterVisible = isCounterVisible;
    }
}
