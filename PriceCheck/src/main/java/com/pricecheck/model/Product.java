package com.pricecheck.model;

import android.graphics.Bitmap;

/**
 * Created by vivek on 2/25/14.
 */
public class Product
{
    private String name;
    private String price;
    private String site;
    private String productLink;
    private String imageUrl;
    private Bitmap actualImage;
    private boolean imageSubmittedForDownload;

    public Product(String name, String price, String site, String productLink, String imageUrl) {
        this.name = name;
        this.price = price;
        this.site = site;
        this.productLink = productLink;
        this.imageUrl = imageUrl;
        sanitizeProduct();
    }

    public void setActualImage(Bitmap actualImage) { this.actualImage = actualImage; }

    public Bitmap getActualImage() { return actualImage; }

    public boolean isActualImageSet() { return actualImage == null; }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public void setProductLink(String productLink) {
        this.productLink = productLink;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {

        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getSite() {
        return site;
    }

    public String getProductLink() {
        return productLink;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String toString()
    {
        return getName() + "|" + getPrice() + "|" + getImageUrl() + "|" + getSite() + "|" +getProductLink();
    }

    public void setImageSubmittedForDownload(boolean imageSubmittedForDownload) {
        this.imageSubmittedForDownload = imageSubmittedForDownload;
    }

    public boolean isImageSubmittedForDownload() {
        return imageSubmittedForDownload;
    }

    private void sanitizeProduct()
    {
        price = price.replaceAll("\\s","");
        if(!price.contains("Rs"))
            price = "Rs." + price;
    }

    public int getIntPrice()
    {
        String priceString = getPrice();
        if(priceString.contains("-"))
            priceString = priceString.split("-")[0];
        priceString = priceString.replaceAll("\\s+", "");
        priceString = priceString.replaceAll("\\u00A0", "");
        priceString = priceString.replace(",", "");
        priceString = priceString.replace("Rs.", "");
        priceString = priceString.replace("Rs", "");

        try{
            return Double.valueOf(priceString).intValue();
        }
        catch (Exception e) {
            return 10000000;
        }
    }
}
