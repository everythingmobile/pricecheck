package com.pricecheck;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import com.pricecheck.fragments.util.UtilLayoutFragment;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by ge3k on 1/5/14.
 */
public class UserTourActivity extends FragmentActivity {
    private static final int PAGE_MAX = 6;
    private ViewPager mViewPager;
    private PriceCheckTutorialAdapter mLaneAdapter;
    
    //TODO: rename these as per our usage.
    private static final int PAGE_WELCOME = 0;
    private static final int PAGE_SEARCH = 1;
    private static final int PAGE_POPUP = 2;
    private static final int PAGE_FILTER = 3;
    private static final int PAGE_ORDER_BY = 4;
    private static final int PAGE_LUCID = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Key the screen from dimming -
        // http://stackoverflow.com/a/4197370/328679
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (mLaneAdapter == null) {
            mLaneAdapter = new PriceCheckTutorialAdapter(getSupportFragmentManager());
        }

        if (mLaneAdapter != null) {
            setContentView(R.layout.tutorial);

            mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(mLaneAdapter);

            CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
            indicator.setViewPager(mViewPager);
            indicator.setStrokeColor(getResources().getColor(R.color.black));
            indicator.setFillColor(getResources().getColor(R.color.black));
            indicator.setOnPageChangeListener(mOnPageChangeListener);
        }
    }

    private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageSelected(int laneIndex) {

            Resources res = getResources();
            String[] titles = res.getStringArray(R.array.tutorial_titles);
            String title = titles[laneIndex];

            invalidateOptionsMenu();
        }
    };


    class PriceCheckTutorialAdapter extends FragmentPagerAdapter {

        public PriceCheckTutorialAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {


            Fragment fragment = null;

            switch (position) {


                case PAGE_WELCOME:
                    fragment =  UtilLayoutFragment
                            .newInstance(R.layout.tour_welcome);
                    break;

                case PAGE_SEARCH:
                    fragment =  UtilLayoutFragment.newInstance(R.layout.tour_search);
                    break;

                case PAGE_POPUP:
                    fragment =  UtilLayoutFragment.newInstance(R.layout.tour_popup);
                    break;

                case PAGE_FILTER:
                    fragment =  UtilLayoutFragment.newInstance(R.layout.tour_filter);
                    break;

                case PAGE_ORDER_BY:
                    fragment =  UtilLayoutFragment.newInstance(R.layout.tour_order_by);
                    break;

                case PAGE_LUCID:
                    fragment =  UtilLayoutFragment.newInstance(R.layout.tour_lucid,
                            new UtilLayoutFragment.Callback() {
                                @Override
                                public void onCreateView(View view) {
                                    Button button = (Button) view.findViewById(R.id.acceptBtn);
                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            onFinishTutorialClicked();
                                        }
                                    });

                                }
                            }
                    );
                    break;

            }
            return fragment;
        }

        public void onFinishTutorialClicked() {
            // We don't want to come back here, so remove from the activity stack
            finish();
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }


        @Override
        public int getCount() {
            return PAGE_MAX;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }
}
