package com.pricecheck.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.pricecheck.Constants;
import com.pricecheck.R;
import com.pricecheck.SearchActivity;
import com.pricecheck.fragments.RecentFragment;
import com.pricecheck.util.Helper;

import java.util.Collections;
import java.util.List;

/**
 * Created by vivek on 4/5/14.
 */
public class RecentAdapter extends ArrayAdapter<String>
{
    private static SearchActivity parentActivity;
    private List<String> items;

    @Override
    public int getCount() {
        return items.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context mContext = parentActivity.getApplicationContext();
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.recent_list_item, null);
        TextView textView = (TextView) view.findViewById(R.id.recentListItem);
        String item = items.get(position);
        textView.setText(item);
        setOnClickListener(view, item);
        return view;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public RecentAdapter(Context context, int resource, SearchActivity parentActivity) {
        super(context, resource);
        this.parentActivity = parentActivity;
        this.items = getRecentItems();
        Collections.reverse(this.items);
    }

    private void setOnClickListener(View view, String text)
    {
        final String query = text;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.displayFragment(SearchActivity.PriceCheckEnum.RESULTS, query);
            }
        });
    }

    private List<String> getRecentItems()
    {
        SharedPreferences preferences = parentActivity.getPreferences(Context.MODE_PRIVATE);
        String queriesString = preferences.getString(Constants.recentString, null);
        return Helper.getQueriesFromString(queriesString);
    }
}
