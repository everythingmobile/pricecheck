package com.pricecheck.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.pricecheck.R;
import com.pricecheck.SearchActivity;
import com.pricecheck.model.RightNavDrawerItem;

import java.util.ArrayList;

public class RightNavDrawerAdapter extends BaseAdapter {

    private static SearchActivity parentActivity;
    private Context context;
	private ArrayList<RightNavDrawerItem> navDrawerItems;
    private static String TAG = "RightNavDrawerAdapter";

    public RightNavDrawerAdapter(SearchActivity searchActivity, ArrayList<RightNavDrawerItem> navDrawerItems){
		this.parentActivity = searchActivity;
        this.context = parentActivity.getApplicationContext();
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }

        CheckBox onOffSwitch = (CheckBox) convertView.findViewById(R.id.on_off_switch);
        ImageView siteImage = (ImageView) convertView.findViewById(R.id.siteImageNavList);
        siteImage.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), navDrawerItems.get(position).getDrawable()));

        onOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    Log.d(TAG,"##### The button " +position + "is checked. Changing the value in the model ####" );
                    navDrawerItems.get(position).setCheckBoxState(true);
                    //buttonView.toggle();
                }
                else {
                    Log.d(TAG,"##### The button " +position + "is unchecked. Changing the value in the model ####" );
                    navDrawerItems.get(position).setCheckBoxState(false);
                    //buttonView.toggle();
                }
                parentActivity.filterChanged();
            }
        });
        
        return convertView;
	}

}
