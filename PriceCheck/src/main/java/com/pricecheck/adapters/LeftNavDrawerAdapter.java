package com.pricecheck.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pricecheck.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek on 4/27/14.
 */
public class LeftNavDrawerAdapter extends ArrayAdapter
{
    private List<String> listNavItems;
    public List<Bitmap> images;
    Context mContext;
    public LeftNavDrawerAdapter(Context context, int resource)
    {
        super(context, resource);
        mContext = context;
        initList();
    }

    private void initList()
    {
        listNavItems = new ArrayList<String>();
        listNavItems.add("Home");
        listNavItems.add("Recent");

        images = new ArrayList<Bitmap>();
        images.add(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_action_home));
        images.add(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_action_recent));
    }

    @Override
    public int getCount() {
        return listNavItems.size();
    }

    @Override
    public int getPosition(Object item) {
        return listNavItems.indexOf(item);
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        String itemName = listNavItems.get(position);
        View ret = inflater.inflate(R.layout.left_nav_item, null);
        TextView textView = (TextView) ret.findViewById(R.id.titleLeftNav);
        ImageView imageView = (ImageView) ret.findViewById(R.id.iconLeftNav);

        textView.setText(itemName);
        imageView.setImageBitmap(images.get(position))
        ;
        return ret;
    }
}
