package com.pricecheck.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.pricecheck.SearchActivity;
import com.pricecheck.fragments.ResultsFragment;
import com.pricecheck.model.Product;
import com.pricecheck.R;
import com.pricecheck.util.Helper;
import com.squareup.picasso.Picasso;

import java.util.*;

/**
 * Created by vivek on 3/2/14.
 */
public class ProductAdapter extends BaseAdapter
{
    private Context mContext;
    private static SearchActivity parentActivity;
    public List<Product> products;
    private List<Product> filteredProducts;
    private int sortingOrder = 0;
    private String TAG = "com.pricecheck.adapters.ProductAdapter";
    private static HashMap<String,Bitmap> siteImages = new HashMap<String, Bitmap>();
    private ResultsFragment parentFragment;
    private List<String> orderList;
    private List<List<Product>> sortingList;
    private Bitmap defaultProductImage;
    private List<Product> autoSuggestList;


    private boolean isMorePressed;

    public ProductAdapter(SearchActivity parentActivity, List<Product> productList, ResultsFragment parentFragment, List<Product> filteredProducts)
    {
        this.parentFragment = parentFragment;
        this.mContext = parentActivity.getApplicationContext();
        defaultProductImage = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.default_product_image);
        this.parentActivity = parentActivity;
        this.filteredProducts = filteredProducts;
        this.products = productList;
        this.sortingList = new ArrayList<List<Product>>();
        sortingList.add(productList);

        if(this.products == null) {
            this.products = new ArrayList<Product>();
        }

        if(this.filteredProducts == null) {
            this.filteredProducts = new ArrayList<Product>();
        }

        setSiteImages();
    }

    private void setSiteImages() {
        siteImages.put("AmazonResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_amazon));
        siteImages.put("EbayResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_ebay));
        siteImages.put("FlipkartResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_flipkart));
        siteImages.put("InfibeamResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_infibeam));
        siteImages.put("NaaptolResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_naaptol));
        siteImages.put("SnapdealResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_snapdeal));
        siteImages.put("TradusResponse", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_tradus));
        siteImages.put("HomeShop18Response", BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_homeshop18));
    }

    public int getCount()
    {
        if (sortingOrder == 4)
            return filteredProducts.size();
        return filteredProducts.size() + 1;
    }

    public List<Product> getProductsToFilter()
    {
        if (sortingOrder == 4)
            return filteredProducts;
        return products;
    }

    public Object getItem(int position)
    {
        return filteredProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Product> getAutoSuggestList() {
        return autoSuggestList;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        TextView productName;
        TextView productPrice;
        ImageView productImage, siteImage;

        //More
        if(position == getCount() - 1 && sortingOrder != 4)
        {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.more_products, null);
            if (isMorePressed && convertView != null) {
                convertView.setVisibility(View.GONE);
                convertView.setClickable(false);
            }
            addListenerToMore(convertView);
            return convertView;
        }

        Product product = (Product) getItem(position);
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.product, null);
        addAppropriateBorder(convertView);

        productName = (TextView)convertView.findViewById(R.id.productName);
        productName.setText(product.getName());

        productPrice = (TextView) convertView.findViewById(R.id.productPrice);
        productPrice.setText(product.getPrice());

        productImage = (ImageView) convertView.findViewById(R.id.productImage);

        Log.d(TAG,"Downloading image.....for position: "+position);

        Picasso.with(parentActivity)
                .load(product.getImageUrl())
                .placeholder(R.drawable.default_product_image)
                .error(R.drawable.default_product_image)
                .into(productImage);

        siteImage = (ImageView) convertView.findViewById(R.id.siteImage);
        siteImage.setImageBitmap(siteImages.get(product.getSite()));
        final Product innerProduct = product;


        addListenerToGridItem(innerProduct, convertView);

        return convertView;
    }

    private void addAppropriateBorder(View convertView) {
        if(sortingOrder == 0) {
            return;
        } else if (sortingOrder == 1) {
            setMyDrawableBackground(convertView, parentActivity.getResources().getDrawable(R.drawable.product_border_sorted_asc));
        } else if (sortingOrder == 2) {
            setMyDrawableBackground(convertView, parentActivity.getResources().getDrawable(R.drawable.product_border_sorted_desc));
        } else if (sortingOrder == 4) {
            setMyDrawableBackground(convertView, parentActivity.getResources().getDrawable(R.drawable.product_border_x));
        }
    }

    private void setMyDrawableBackground(View view, Drawable drawable)
    {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    private void addListenerToMore(View view)
    {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {
                parentFragment.getProducts(null);
                convertView.setVisibility(View.GONE);
                convertView.setClickable(false);
                isMorePressed = true;
            }
        });
    }

    private void addListenerToGridItem(final Product innerProduct, View convertView)
    {
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(parentActivity, R.style.ProductPopoverDialog);
                dialog.setContentView(R.layout.product_expanded);
                setDialogView(dialog, innerProduct);
                dialog.show();
            }

            public void setDialogView(Dialog dialogView,final Product innerProduct)
            {
                TextView nameView = (TextView) dialogView.findViewById(R.id.productName);
                TextView priceView = (TextView) dialogView.findViewById(R.id.productPrice);
                ImageView siteImage = (ImageView) dialogView.findViewById(R.id.siteImage);
                ImageView productImageView = (ImageView) dialogView.findViewById(R.id.productImage);

                nameView.setText(innerProduct.getName());
                priceView.setText(innerProduct.getPrice());
                siteImage.setImageBitmap(ProductAdapter.siteImages.get(innerProduct.getSite()));

                Picasso.with(parentActivity)
                        .load(innerProduct.getImageUrl())
                        .placeholder(R.drawable.default_product_image)
                        .error(R.drawable.default_product_image)
                        .into(productImageView);


                Helper.lockScreenRotation(parentActivity);
                dialogView.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Helper.unLockScreenRotation(parentActivity);
                    }
                });
                dialogView.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Helper.unLockScreenRotation(parentActivity);
                    }
                });

                Button visitButton = (Button) dialogView.findViewById(R.id.productExpandedVisitSite);
                visitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(innerProduct.getProductLink()));
                        parentActivity.startActivity(browserIntent);
                    }
                });

                Button backButton = (Button) dialogView.findViewById(R.id.productExpandedBack);
                final Dialog finalDialog = dialogView;
                backButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finalDialog.dismiss();
                    }
                });
            }
        });
    }


    public void addProducts(List<Product> products)
    {
        this.products.addAll(products);
        this.sortingList.add(products);
        this.filteredProducts.addAll(parentActivity.getFilteredProducts(products));
        if(sortingOrder == 0){
            notifyDataSetChanged();
        } else if (sortingOrder == 1) {
            sortByPrice(2);
        } else if(sortingOrder == 2){
            sortByPrice(3);
        }
    }

    public void notifyOnFilter(List<Product> filteredProducts)
    {
        this.filteredProducts = filteredProducts;
        notifyDataSetChanged();
    }

    public void sortByPrice(int position)
    {
        products = new ArrayList<Product>();
        for(List<Product> productList : this.sortingList)
        {
            if(position == 0)
                sortListBySite(productList);
            else if (position == 1) {
                invocateFeatureX();
                return;
            }
            else if (position == 2)
                sortListByPriceAscending(productList);
            else
                sortListByPriceDescending(productList);
            products.addAll(productList);
        }
        filteredProducts = parentActivity.getFilteredProducts(products);
        notifyDataSetChanged();
    }

    private void sortListBySite(List<Product> productList) {
        sortingOrder = 3;
        initOrderList();
        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return orderList.indexOf(lhs.getSite()) - orderList.indexOf(rhs.getSite());
            }
        });
    }

    private void invocateFeatureX()
    {
        sortingOrder = 4;
        if(autoSuggestList == null)
            filteredProducts = new ArrayList<Product>();
        filteredProducts = parentActivity.getFilteredProducts(autoSuggestList);
        notifyDataSetChanged();
    }

    private void sortListByPriceDescending(List<Product> productList)
    {
        sortingOrder = 2;
        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return rhs.getIntPrice() - lhs.getIntPrice();
            }
        });
    }

    private void sortListByPriceAscending(List<Product> productList)
    {
        sortingOrder = 1;
        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return lhs.getIntPrice() - rhs.getIntPrice();
            }
        });
    }

    private void initOrderList()
    {
        if(orderList == null) {
            orderList = new ArrayList<String>();
            orderList.add("FlipkartResponse");
            orderList.add("AmazonResponse");
            orderList.add("TradusResponse");
            orderList.add("SnapdealResponse");
            orderList.add("InfibeamResponse");
            orderList.add("NaaptolResponse");
            orderList.add("EbayResponse");
            orderList.add("HomeShop18Response");
        }
    }

    public boolean isMorePressed() {
        return isMorePressed;
    }

    public void setMorePressed(boolean isMorePressed) {
        this.isMorePressed = isMorePressed;
    }


    public void setAutoSuggestList(List<Product> autoSuggestList) {
        this.autoSuggestList = autoSuggestList;
    }
}
