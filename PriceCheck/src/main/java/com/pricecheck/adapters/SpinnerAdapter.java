package com.pricecheck.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.pricecheck.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vivek on 4/19/14.
 */
public class SpinnerAdapter extends ArrayAdapter
{
    private List<String> spinnerElements;
    private Context mContext;

    public SpinnerAdapter(Context context, int resource, Object[] objects)
    {
        super(context, resource, context.getResources().getStringArray(R.array.action_list));
        mContext = context;
        spinnerElements = new ArrayList<String>(Arrays.asList(context.getResources().getStringArray(R.array.action_list)));
    }

    @Override
    public int getCount() {
        return spinnerElements.size();
    }

    @Override
    public int getPosition(Object item) {
        return spinnerElements.indexOf(item);
    }

    @Override
    public Object getItem(int position) {
        return spinnerElements.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        String name = spinnerElements.get(position);
        TextView textView = (TextView) inflater.inflate(R.layout.spinner_element, null);
        textView.setText(name);
        textView.setTextColor(Color.parseColor("#ff232227"));
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        String name = spinnerElements.get(position);
        TextView textView = (TextView) inflater.inflate(R.layout.spinner_element, null);
        textView.setText(name);
        if (position == 1) {
            textView.setTextColor(Color.parseColor("#41DAFF"));
            textView.setBackgroundColor(Color.parseColor("#f0f0f0"));
            textView.setTypeface(null, Typeface.BOLD);
        }
        else
        {
            textView.setTextColor(Color.parseColor("#ff232227"));
        }
        return textView;
    }
}
