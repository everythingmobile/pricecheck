package com.pricecheck.Service;

import com.pricecheck.adapters.ProductAdapter;
import com.pricecheck.model.RightNavDrawerItem;
import com.pricecheck.model.Product;
import com.pricecheck.util.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vivek on 4/13/14.
 */
public class FilteringService
{
    private List<RightNavDrawerItem> navDrawerItemList;
    private Map<String, Boolean> isSitePresent;
    private Map<Helper.SITES, String> navBarItemName = new HashMap<Helper.SITES, String>();
    private List<ProductAdapter> listeners = new ArrayList<ProductAdapter>();

    public FilteringService(List<RightNavDrawerItem> navDrawerItemList)
    {
        this.navDrawerItemList = navDrawerItemList;
        navBarItemName.put(Helper.SITES.FLIPKART, "FlipkartResponse");
        navBarItemName.put(Helper.SITES.AMAZON, "AmazonResponse");
        navBarItemName.put(Helper.SITES.NAAPTOL, "NaaptolResponse");
        navBarItemName.put(Helper.SITES.SNAPDEAL, "SnapdealResponse");
        navBarItemName.put(Helper.SITES.EBAY, "EbayResponse");
        navBarItemName.put(Helper.SITES.INFIBEAM, "InfibeamResponse");
        navBarItemName.put(Helper.SITES.TRADUS, "TradusResponse");
        navBarItemName.put(Helper.SITES.HOMESHOP18, "HomeShop18Response");
        setMap(navDrawerItemList);
    }

    private void setMap(List<RightNavDrawerItem> navDrawerItemList)
    {
        isSitePresent = new HashMap<String, Boolean>();
        for(RightNavDrawerItem navDrawerItem : navDrawerItemList)
        {
            if(navDrawerItem.isCheckBoxState())
                isSitePresent.put(navBarItemName.get(navDrawerItem.getSite()), true);
            else
                isSitePresent.put(navBarItemName.get(navDrawerItem.getSite()), false);
        }
    }

    public void registerListener(ProductAdapter productAdapter)
    {
        listeners.add(productAdapter);
    }

    public List<Product> getFilteredProducts(List<Product> productsToBeFiltered)
    {
        List<Product> ret = new ArrayList<Product>();
        for(Product product : productsToBeFiltered)
        {
            try{
                String site = product.getSite();
                if(isSitePresent.get(site) == null)
                    System.out.println(site + " " +productsToBeFiltered.indexOf(product));
                if(isSitePresent.get(site))
                {
                    ret.add(product);
                }
            } catch (Exception e) {
                continue;
            }
        }
         return ret;
    }

    public void filterChangeNotification(List<RightNavDrawerItem> navDrawerItemList)
    {
        setMap(navDrawerItemList);
        notifyListeners();
    }

    private void notifyListeners()
    {
        for(ProductAdapter productAdapter : listeners)
        {
            productAdapter.notifyOnFilter(getFilteredProducts(productAdapter.getProductsToFilter()));
        }

    }
}
