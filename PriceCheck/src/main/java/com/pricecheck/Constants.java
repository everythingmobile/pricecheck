package com.pricecheck;

import android.app.Application;

/**
 * @author: Aishraj
 * Date: 26/2/14
 * Time: 8:27 AM
 * Class to hold global application level constants
 */
public class Constants extends Application{
    public static final String FIRST_RUN = "com.priceheck.firstrun";
    public static final String FIRST_RUN_KEY = "com.pricecheck.firstrun.KEY";
    public static final String APPTAG = "com.pricecheck";
    public static final String SEARCH_BUNDLE_KEY = "com.pricecheck.search_bundle_key";
    public static final String recentString = "com.pricecheck.RecentFragment.RECENT_KEY";
    //Add all other application level constants (such as intent action strings here)
}
